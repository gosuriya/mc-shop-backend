export interface IResponse {
  results: boolean;
  statusCode: number;
  statusMessages: string;
  data?: any[];
}
