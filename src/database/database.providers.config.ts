import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const SQLOrmConfig: TypeOrmModuleOptions = {
  type: 'mysql',
  host: '127.0.0.1',
  port: 3306,
  username: 'root',
  password: '',
  database: 'mc_shop',
  timezone: 'Asia/Bangkok',
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: true,
  extra: {
    charset: 'utf8_general_ci',
  },
};

console.log(SQLOrmConfig);
