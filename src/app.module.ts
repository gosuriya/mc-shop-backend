import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SQLOrmConfig } from './database/database.providers.config';
import { ProductModule } from './product/product.module';

@Module({
  imports: [TypeOrmModule.forRoot(SQLOrmConfig), ProductModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
