import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'product' })
export class ProductEntity {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  id?: number;

  @Column({ name: 'count', type: 'varchar', length: 255 })
  count: string;

  @Column({ name: 'price', type: 'varchar', length: 255 })
  price: string;

  @Column({ name: 'img', type: 'varchar', length: 255 })
  img: string;
}
