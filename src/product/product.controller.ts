import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Post,
  Put,
} from '@nestjs/common';
import { IResponse } from 'src/interfaces/response.interface';
import { ProductDto } from './dto/product-request-dto';
import { ProductService } from './product.service';

// @ApiTag('')
@Controller('product')
export class ProductController {
  private readonly logger = new Logger('ProductService');
  constructor(private readonly productService: ProductService) {}

  @Post('/createProduct')
  async createProduct(@Body() body: ProductDto): Promise<IResponse> {
    const res = await this.productService.createProduct(body);
    this.logger.verbose(
      `Create to product for "${res}". Data product : ${JSON.stringify(body)}`,
    );
    return res;
  }

  @Get('/getProducts')
  async getProducts(): Promise<IResponse> {
    const data = await this.productService.getProducts();
    this.logger.verbose(
      `Get to products for "${data}". Data product : ${JSON.stringify(data)}`,
    );
    return data;
  }

  @Get('/getProducts/:id')
  async getProductById(id: number): Promise<IResponse> {
    const data = await this.productService.getProductById(id);
    this.logger.verbose(
      `Get to product id for "${data}". Data product : ${JSON.stringify(id)}`,
    );
    return data;
  }

  @Put('/products/:id')
  async updatedProductById(body: ProductDto): Promise<IResponse> {
    const data = await this.productService.updatedProductById(body);
    this.logger.verbose(
      `Put to product for "${data}". Data product : ${JSON.stringify(body)}`,
    );
    return data;
  }

  @Delete('/product/:id')
  async deleteProduct(id: number): Promise<IResponse> {
    const data = await this.productService.deleteProduct(id);
    this.logger.verbose(
      `Delete to product for "${data}". Data product : ${JSON.stringify(id)}`,
    );
    return data;
  }
}
