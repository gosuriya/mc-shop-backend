export class ProductDto {
  id?: number;

  count: string;

  price: string;

  img: string;
}
