import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IResponse } from 'src/interfaces/response.interface';
import { Repository } from 'typeorm';
import { ProductDto } from './dto/product-request-dto';
import { ProductEntity } from './entities/product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductEntity)
    private readonly productRepository: Repository<ProductEntity>,
  ) {}

  async createProduct(body: ProductDto) {
    try {
      const product = new ProductEntity();
      product.count = body.count;
      product.price = body.price;
      product.img = body.img;
      this.productRepository.save(product);

      return ({
        results: true,
        statusCode: HttpStatus.CREATED,
        statusMessages: '',
        data: product,
      } as unknown) as IResponse;
    } catch (e) {
      throw new HttpException(
        {
          results: false,
          statusCode: HttpStatus.NOT_IMPLEMENTED,
          statusMessages: '',
        } as IResponse,
        HttpStatus.NOT_IMPLEMENTED,
      );
    }
  }

  async getProducts(): Promise<IResponse> {
    try {
      const data = await this.productRepository.find({
        select: ['id', 'count', 'price', 'img'],
        // where: { id },
      });

      return ({
        results: true,
        statusCode: HttpStatus.OK,
        statusMessages: '',
        data: data,
      } as unknown) as IResponse;
    } catch (e) {
      throw new HttpException(
        {
          results: false,
          statusCode: HttpStatus.NOT_FOUND,
          statusMessages: '',
        } as IResponse,
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async getProductById(id: number) {
    try {
      const found = await this.productRepository.findOne(id);

      if (!found) {
        throw new NotFoundException(`Product ${id} is not found!`);
      }

      return ({
        results: true,
        statusCode: HttpStatus.OK,
        statusMessages: '',
        data: found,
      } as unknown) as IResponse;
    } catch (e) {
      throw new HttpException(
        {
          results: false,
          statusCode: HttpStatus.NOT_FOUND,
          statusMessages: '',
        } as IResponse,
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async updatedProductById(body: ProductDto) {
    try {
      const updatedProduct = await this.productRepository.findOne(body.id);

      if (!updatedProduct) {
        throw new NotFoundException(`Product ${body.id} is not found!`);
      }

      updatedProduct.count = body.count;
      updatedProduct.price = body.price;
      updatedProduct.img = body.img;
      this.productRepository.save(updatedProduct);

      return ({
        results: true,
        statusCode: HttpStatus.CREATED,
        statusMessages: '',
        data: updatedProduct,
      } as unknown) as IResponse;
    } catch (error) {
      throw new HttpException(
        {
          results: false,
          statusCode: HttpStatus.NOT_IMPLEMENTED,
          statusMessages: '',
        } as IResponse,
        HttpStatus.NOT_IMPLEMENTED,
      );
    }
  }

  async deleteProduct(id: number) {
    try {
      const result = await this.productRepository.delete(id);

      if (result.affected === 0) {
        throw new NotFoundException(`Product with id : ${id} not found`);
      }

      return ({
        results: true,
        statusCode: HttpStatus.NO_CONTENT,
        statusMessages: '',
        data: result,
      } as unknown) as IResponse;
    } catch (e) {
      throw new HttpException(
        {
          results: false,
          statusCode: HttpStatus.NOT_IMPLEMENTED,
          statusMessages: '',
        } as IResponse,
        HttpStatus.NOT_IMPLEMENTED,
      );
    }
  }
}
